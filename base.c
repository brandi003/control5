#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"
#include <math.h>
#include <string.h>


float desvStd(estudiante curso[]){
	float promg = 0, base, diferencia, diferencia2, desviacion = 0;
	int i = 0, cont = 0, suma;
	while(curso[i].prom != 0.0){
		promg += curso[i].prom;
		i++;
		cont++;
	}
	if(cont == 0){
		printf("no se han ingresado notas\n");
		return 0;
	}
	promg = promg/cont;
	

	i = 0;
	while (i<cont){
		diferencia = curso[i].prom -promg;
		diferencia2 = diferencia*diferencia;
		suma += diferencia2;
		i++;
	}
	base = suma/cont;
	desviacion= sqrt(base);
	return desviacion;
}

float menor(estudiante curso[]){
	float menor = 7.0;
	int i = 0,cont=0;
	while(strcmp(curso[i].nombre,"\0") != 0){
		i++;
		cont++;
	i=0;
	int alumnos =cont;
	for(i= 0;i<alumnos;i++){
		if(menor > curso[i].prom && curso[i].prom != 0 ){
			menor = curso[i].prom;
		}
	}
	return menor;
}
}

float mayor(estudiante curso[]){
	float mayor = 1.0;
	int i = 0;
	while(curso[i].prom != 0){
		if(mayor<curso[i].prom){
			mayor = curso[i].prom;
		}
		i++;
	}
	return mayor;
}

void registroCurso(estudiante curso[]){    //en esta funcion se piden las notas del alumno y se hace un promedio para despues guardarlo
	int i,prom[24],prom_parcial;
	for (i=0;i<24;i++){
		curso[i].prom=0;
		printf("para el estudiante %s %s %s:\n",curso[i].nombre,curso[i].apellidoP,curso[i].apellidoM);
		printf("ingrese la nota del proyecto 1\n");
		scanf("%f",&curso[i].asig_1.proy1);
		prom_parcial=prom_parcial+(curso[i].asig_1.proy1 * 0.20);
		printf("ingrese la nota del proyecto 2\n");
		scanf("%f",&curso[i].asig_1.proy2);
		prom_parcial=prom_parcial+(curso[i].asig_1.proy2 * 0.20);
		printf("ingrese la nota del proyecto 3\n");
		scanf("%f",&curso[i].asig_1.proy3);
		prom_parcial=prom_parcial+(curso[i].asig_1.proy3 * 0.30);
		printf("ingrese la nota del control 1\n");
		scanf ("%f",&curso[i].asig_1.cont1);
		prom_parcial=prom_parcial+(curso[i].asig_1.cont1 * 0.05);
		printf("ingrese la nota del control 2\n");
		scanf ("%f",&curso[i].asig_1.cont2);
		prom_parcial=prom_parcial+(curso[i].asig_1.cont2 * 0.05);
		printf("ingrese la nota del control 3\n");
		scanf ("%f",&curso[i].asig_1.cont2);
		prom_parcial=prom_parcial+(curso[i].asig_1.cont3 * 0.05);
		printf("ingrese la nota del control 4\n");
		scanf ("%f",&curso[i].asig_1.cont2);
		prom_parcial=prom_parcial+(curso[i].asig_1.cont4 * 0.05);
		printf("ingrese la nota del control 5\n");
		scanf ("%f",&curso[i].asig_1.cont2);
		prom_parcial=prom_parcial+(curso[i].asig_1.cont5 * 0.05);
		printf("ingrese la nota del control 6\n");
		scanf ("%f",&curso[i].asig_1.cont2);
		prom_parcial=prom_parcial+(curso[i].asig_1.cont6 * 0.05);
		prom[i]=prom_parcial;

		}
}

void clasificarEstudiantes(char path[], estudiante curso[]){
	FILE *rep;		//se crean los punteros
	FILE *apr;
	char lista_reprobados[1024]="",lista_aprobados[1024]="";
	int i=0,True=1;
	apr=fopen("aprobados.txt","w");			//se crean los documentos donde se guardaran los aprobados y reprobados
	rep=fopen("reprobados.txt","w");
	while(True){
		if (curso[i].prom != 0.0){
			break;
		}
		if (curso[i].prom< 4.0 && curso[i].prom != 0){
			strcat(lista_reprobados ,curso[i].nombre);
			strcat(lista_reprobados, " ");
			strcat(lista_reprobados ,curso[i].apellidoP);
			strcat(lista_reprobados, " ");
			strcat(lista_reprobados, curso[i].apellidoM);
			strcat(lista_reprobados, " ");
			fprintf(rep,"%s %2f\n",lista_reprobados,curso[i].prom);
			strcpy(lista_aprobados,"");
		}
		else{
			strcat(lista_aprobados ,curso[i].nombre);
			strcat(lista_aprobados, " ");
			strcat(lista_aprobados ,curso[i].apellidoP);
			strcat(lista_aprobados, " ");
			strcat(lista_aprobados, curso[i].apellidoM);
			strcat(lista_aprobados, " ");
			fprintf(apr, "%s %2f\n",lista_aprobados,curso[i].prom);
			strcpy(lista_reprobados,"");}
	}
}


void metricasEstudiantes(estudiante curso[]){
	float desviacion=desvStd(curso),prom_mayor=mayor(curso),prom_menor=menor(curso);
	printf("\n la desviacion estandar es: %3f\n el mejor promedio fue:%2f\nel menor promedio fue: %2f\n",desviacion,prom_mayor,prom_menor);
}


void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes("destino", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}

int main(){
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}