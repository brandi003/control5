#include "estudiantes.h"
#include <stdio.h>
#include<stdlib.h>
void cargarEstudiantes(char path[], estudiante curso[]){
	FILE *file;
	int i = 0;
	if((file=fopen(path,"r"))==NULL){
		printf("\nerror al arbir el archivo");
		exit(0); 
	}
	else{
		printf("\nLos estudiantes cargados son: \n");
		while (feof(file) == 0) {
			fscanf(file,"%s %s %s", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM);
			printf("%s %s %s\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM);
			i++;

		}
		fclose(file);
	} 
}

void grabarEstudiantes(char path[], estudiante curso[]){
	FILE *file;
	if((file=fopen(path,"w"))==NULL){
		printf("\nerror al crear el archivo");
		exit(0); 
	}
	else{
		for (int i = 0; i<24; i++)
			fprintf(file,"%s\t%s\t%s\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t %.1f\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM, curso[i].asig_1.proy1, curso[i].asig_1.proy2, curso[i].asig_1.proy3,
 curso[i].asig_1.cont1, curso[i].asig_1.cont2, curso[i].asig_1.cont3, curso[i].asig_1.cont4, curso[i].asig_1.cont5, curso[i].asig_1.cont6, curso[i].prom);
		
	} 
	printf("\n **El registro de estudiantes fue almacenado de manera correcta**\n \n ");
	fclose(file);
}

